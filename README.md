# Design Dream

## Overview

Welcome to **Design Dream**, an exciting and heartwarming game that blends the thrill of puzzle-solving with the joy of home decoration. Follow Ivy's journey as she prepares for an important visit from her parents, transforming her house from a disheveled mess into a cozy, welcoming home.

## Storyline

Ivy's parents are coming to visit her for the first time in years, and she wants everything to be perfect. However, her house is far from ready. There are broken utensils, wobbly chairs, cracked tables, and a plethora of other problems that need fixing. Determined to make her home presentable, Ivy embarks on a mission to repair and furnish her house, one piece at a time.

## Gameplay

### Earn Points by Solving Puzzles

To help Ivy fix her house, players must earn points by solving various engaging and challenging puzzles. These puzzles include:

- **Wordle Wizardry**: Test your vocabulary and deduction skills by guessing the correct word within a limited number of attempts. Each successful word guessed earns points that can be used to repair and upgrade different parts of Ivy's home.

- **Match Magic**: Combine strategy and quick thinking to match three or more identical items in a row. Completing these puzzles rewards players with points and special bonuses to speed up Ivy's home improvement project.

### Home Decoration

As players accumulate points, they can choose which items to repair or purchase new furniture and decorations. The game features a range of customization options, allowing players to create a home that reflects their personal style while meeting Ivy's needs.

### Walkthrough

- (video)
- (screenshots)

## Key Features

- **Engaging Puzzles**: Enjoy a variety of puzzles that challenge your mind and keep you entertained.
- **Creative Customization**: Decorate Ivy's home with a vast selection of furniture and decor items, ensuring each room is unique and inviting.
- **Heartwarming Storyline**: Follow Ivy's emotional journey as she reconnects with her parents through her efforts to create a perfect home.
- **Rewarding Progression**: Earn points and rewards for completing puzzles, making visible progress in the game's storyline and home improvement goals.

## Tech Stacks

- Programming Language -> C#
- Game Development Engine -> Unity

## Installation

- Unity 6 version 6000.0.8f1

## Usage
- Clone the repository

``` git clone https://gitlab.com/we-team3/mission-impractical.git ```
- Navigate to the directory

``` cd mission-impractical```

- Open in Unity 6.

## Resources

- For learning Unity: https://docs.unity3d.com/Manual/index.html 
- For learning C#: https://docs.microsoft.com/en-us/dotnet/csharp/




